import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Driver {

	public static void main(String[] args) {
		
		Graph g = new Graph();
		File inputFile = null;
		if(args.length > 0)
			inputFile = new File(args[0]);
		else
			inputFile = new File("list.txt");
		Scanner infile = null;
		try 
		{
			infile = new Scanner(inputFile);
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println(e.getMessage());
		}
		
		//int numberOfEdges = infile.nextInt();
		
		while(infile.hasNextLine())
		{
			StringTokenizer st = new StringTokenizer(infile.nextLine());
			if(st.countTokens() != 2)
			{
				//do nothing
				//it doesn't have an edge
			}
			else
			{
				Integer source = Integer.parseInt(st.nextToken());
				Integer dest = Integer.parseInt(st.nextToken());
				
				g.addEdge(source, dest);
			}
		}
		
		String is_acyclic = g.isAcyclic() ? "is acyclic":"is NOT acyclic";
		List<Integer> sortedGraph = g.topologicalSort();
		String strGraph = "Cannot sort because graph is cyclic.\n"+g.toString();
		if(sortedGraph != null)
			strGraph = sortedGraph.toString();
		
		System.out.println("Graph " + is_acyclic +  
				"\tSorted Vertices: "+strGraph);

		//System.out.println(g.toString());

	}

}
