import java.util.*;

public class Graph 
{
	private Map<Integer, List<Integer>> adj =
			new HashMap<Integer, List<Integer>>();
	
	public void addEdge ( Integer i )
	{
		if(adj.containsKey(i))
			return;
		adj.put(i, new ArrayList<Integer>());
	}
	
	public void addEdge ( Integer source, Integer dest )
	{
		this.addEdge(source);
		this.addEdge(dest);
		adj.get(source).add(dest);
	}
	
	public boolean contains (Integer i)
	{
		return adj.containsKey(i);
	}
	
	public Map<Integer, Integer> inDegree()
	{
		Map<Integer, Integer> indegree = 
				new HashMap<Integer, Integer>();
		for(Integer i : adj.keySet())
			indegree.put(i, 0);
		for(Integer source: adj.keySet())
			for(Integer dest: adj.get(source))
				indegree.put(dest, indegree.get(dest)+1);
		return indegree;
	}
	
	public List<Integer> topologicalSort()
	{
		Map<Integer, Integer> indegree = inDegree(); //this.inDegree???
		Stack<Integer> vertices = new Stack<Integer>();
		for(Integer i : indegree.keySet())
			if(indegree.get(i) == 0)
				vertices.push(i);
		ArrayList<Integer> result = new ArrayList<Integer>();
		while(!vertices.isEmpty())
		{
			Integer temp = vertices.pop();
			result.add(temp);
			for(Integer i: adj.get(temp))
			{
				indegree.put(i, indegree.get(i) - 1);
				if(indegree.get(i) == 0)
					vertices.push(i);
			}
		}
		if(result.size() != adj.size())
			return null;
		return result;
	}
	
	public boolean isAcyclic()
	{
		return topologicalSort() != null;
	}
	
	public String toString()
	{
		StringBuffer result = new StringBuffer();
		for(Integer i: adj.keySet())
			result.append( "\n "+ i +" { " + adj.get(i)+ " } ");
		return result.toString();
	}
}
