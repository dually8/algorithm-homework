import java.io.BufferedReader;
import java.util.*;
/**
 * 
 */

/**
 * @author CJ
 *
 */
public class Graph_bak {

	/**
	 * 
	 */
	
	private class Vertex
	{
		private Integer source;
		private ArrayList<Vertex> adj;
		private int dist;
		Vertex path;
		
		public Vertex(Integer i)
		{
			source = i;
			adj = new ArrayList<Vertex>(10);
			reset();
		}
		
		public void reset()
		{
			dist = Graph.INFINITY;
			path = null;
		}
	}
	
	private ArrayList<Vertex> vlist;
	public static final int INFINITY = Integer.MAX_VALUE;
	
	public Graph() {
	}
	
	private Vertex getVertex(Integer vert)
	{
		Vertex v = vlist.get(vert);
		if(v == null)
		{
			v = new Vertex(vert);
			vlist.add(vert, v);
		}
		return v;
	}
	
	public void addEdge( Integer source, Integer dest )
	{
		Vertex v = getVertex(source);
		Vertex w = getVertex(dest);
		v.adj.add(w);
	}
	
	public void printPath(Integer dest) throws  NoSuchElementException
	{
		Vertex w = vlist.get(dest);
		if( w == null )
			throw new NoSuchElementException("Destination vertex not found");
		else if( w.dist == INFINITY )
			System.out.println(dest + " is unreachable");
		else
		{
			printPath(w);
			System.out.print("\n");
		}
	}
	
	private void printPath(Vertex dest)
	{
		if(dest.path != null)
		{
			printPath(dest.path);
			System.out.print(" to ");
		}
		System.out.print(dest.source);
	}
	
	private void clear()
	{
		vlist.clear();
	}
	
	public void unweighted(Integer start) throws NoSuchElementException
	{
		clear();
		
		Vertex vert = vlist.get(start);
		if(vert == null)
			throw new NoSuchElementException("Start vertex not found");
		ArrayList<Vertex> q = new ArrayList<>();
		q.add(vert);
		vert.dist = 0;
		
		while(!q.isEmpty())
		{
			Vertex v = (Vertex) q.remove(0);
			
			for(Iterator it = v.adj.iterator(); it.hasNext();)
			{
				Vertex w = (Vertex) it.next();
				if(w.dist == INFINITY)
				{
					w.dist = v.dist+1;
					w.path = v;
					q.add(w);
				}
			}
		}
	}
}
