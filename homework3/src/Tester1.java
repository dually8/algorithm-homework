

import java.util.LinkedList;
import java.util.Random;

public class Tester1 {

	public static void main(String[] args){
        LinkedList<Integer> list = new LinkedList<Integer>();
        fillList(list);
        System.out.println("List of twenty random values from 0 to 1000:");
        printList(list);
        System.out.println("\n-----------");
        System.out.println("Odds removed:");
        printList(iterator(list));       
	}
	public static LinkedList<Integer> iterator(LinkedList<Integer> intList){
	        LinkedList<Integer> evenList = new LinkedList<Integer>();
	        for(int i = 0; i <= intList.size() - 1; i++){
	                if((intList.get(i)%2) == 0){
	                        evenList.add(intList.get(i));
	                }
	                else if((intList.get(i)%2) == 1){
	                        //odd numbers not added to the even list!
	                }
	                else{
	                        System.out.println("error");
	                }
	        }
	        return evenList;
	}
	
	public static void fillList(LinkedList<Integer> intList){
	        Random rand = new Random();
	        for(int i = 0; i <= 19; i++){
	                int value = rand.nextInt(1000);
	                intList.add(value);
	        }
	}
	public static void printList(LinkedList<Integer> intList){
	        for(int i = 0; i <= intList.size() - 1; i++){
	                System.out.print(intList.get(i) + " ");
	        }
	}

}
