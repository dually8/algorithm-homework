

import java.awt.Rectangle;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class Tester2 {

	public static void main(String[] args) {
		
		class RectangleComparator implements Comparator
		{
			public int compare(Object arg0, Object arg1) 
			{
				Rectangle r1 = (Rectangle) arg0;
				Rectangle r2 = (Rectangle) arg1;
				if (r1.getWidth() < r2.getWidth())
				{
				   return -1;
				}
				if (r1.getWidth() > r2.getWidth())
				{
				   return 1;
				}
				else
				{
					return 0;
				}
			}
		}
		
		RectangleComparator rComp = new RectangleComparator();
		TreeSet<Rectangle> tree = new TreeSet<Rectangle>(rComp);
		tree.add(new Rectangle(1,1,1,1));
		tree.add(new Rectangle(2,2,2,2));
		tree.add(new Rectangle(3,3,3,3));
		tree.add(new Rectangle(5,5,5,5));
		tree.add(new Rectangle(4,4,4,4));
		tree.add(new Rectangle(3,3,3,3));
		tree.add(new Rectangle(5,5,5,5));
		
		Iterator<Rectangle> iterator = tree.iterator();
		System.out.println("TreeSet data: ");
		
		while(iterator.hasNext())
		{
				System.out.println("\t"+iterator.next().toString());;
		}
		System.out.println("/////////////////");
		
		if(tree.isEmpty())
			System.out.println("TreeSet is empty.");
		else
			System.out.println("TreeSet size : " + tree.size());
	}

}
