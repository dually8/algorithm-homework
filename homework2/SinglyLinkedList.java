import java.util.EmptyStackException;
import java.util.NoSuchElementException;

// Craig Tanis
// Fri Jan 31 2014

// Generic singly linked list
public class SinglyLinkedList<T>
{
	
	private int counter;

    // inner class representing a single list node.  Since the class is
    // private, only the code in this SinglyLinkedList class can access it
    private class SLNode
    {
        public SLNode(){}

        public SLNode(T val, SLNode n)
        {
            value = val;
            next = n;
        }

        public T value;         // current value
        public SLNode next;     // reference to next node in list
    }


    private SLNode head;        // root/head node of list


    public SinglyLinkedList()
    {
        head = new SLNode();    // the head is a dummy node used to facilitate
        counter = 0;            // the use of iterators
    }

    
    // splice a new node in at the head
    public void add(T value)
    {
        head.next = new SLNode(value, head.next);
    }


    public int size()
    {
        int count=0;
        SLNode curs = head;

        // count how many times we can hop references
        while (curs.next != null)
        {
            curs = curs.next;
            count++;
        }

        return count;
    }


    // 
    public class Iterator
    {
        private SLNode node;    // could be a WeakReference<SLNode> with
                                // little modification

        // private constructor means that even though the inner class is
        // exposed to the public, new instances cannot be created
        private Iterator(SLNode n)
        {
            node = n;
        }


        public boolean hasNext()
        {
            return node.next != null;
        }

        // this advances the iterator and returns the value at the next node
        // precondition: hasNext() returns true
        public T next()
        {
            node = node.next;

            if (node != null)
            {
                return node.value;
            }
            else
            {
                return null;
            }
        }

        // splice a new value into the list after the iterator's position
        // following this call, the use of =next()= will advance the iterator
        // to the newly inserted value
        public void add(T newval)
        {
            node.next=new SLNode(newval, node.next);
        }
    }


    // return a new iterator sitting on the dummy head node, so that the first
    // call to =next()= will iterate to the first node and return the first
    // list value
    public Iterator getIterator()
    {
        return new Iterator(head);
    }
    
    
    public class StackList
    {
        private SLNode head;

        private class SLNode
    	{
    		private T value;
    		private SLNode next;

    		public SLNode(T o, SLNode n)
    		{
    			value = o;
    			next = n;
    		}
        }

        public StackList()
    	{
    		head = null;
        }

        //public interface methods - push pop top empty clear
        public void push(T o)
    	{
    		head = new SLNode(o, head);
        }

        public T pop()throws NullPointerException
    	{
    		if(head!=null)
    		{
    			T top = head.value;
    			head = head.next;
    			return top;
    		}
    		else
    		{
    			throw new NullPointerException("Stack Underflow");
    		}
        }

        public T top() throws NullPointerException
        {
    	    
    		    if(head != null)
    		        return(head.value);
    		    else
    		    	throw new NullPointerException("head is null");
        }

        public boolean empty()
    	{
    		return (head == null);
        }

        public void clear()
    	{
    		head = null;
        }
        
    }
    
    public StackList getStack()
    {
    	return new StackList();
    }
    
    
    
	
}

