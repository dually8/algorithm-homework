public class StackList<T>
    {
        private SLNode head;

        private class SLNode
    	{
    		private T value;
    		private SLNode next;
    		private int index;

    		public SLNode(T o, SLNode n)
    		{
    			value = o;
    			next = n;
    			index = 0;
    		}
    		
    		public void addIndex()
    		{
    			this.index += 1;
    		}
    		
    		public void subtractIndex()
    		{
    			this.index -= 1;
    		}
    		
        }

        public StackList()
    	{
    		head = null;
        }
        
        public SLNode getHead()
        {
        		return head;
        }

        //public interface methods - push pop top empty clear
        public void push(T o)
    	{
    		head = new SLNode(o, head);
    		SLNode curs = head;
		while (curs.next != null)
		{
//		 	curs.addIndex();
		        curs = curs.next;
		        curs.addIndex();
	        }
        }

        public T pop()throws NullPointerException
    	{
    		if(head!=null)
    		{
    			SLNode curs = head;
	    		while (curs.next != null)
		        {
//			        	curs.subtractIndex();
			        	curs = curs.next;
			        	curs.subtractIndex();
		        }
    			T top = head.value;
    			head = head.next;    			
    			return top;
    		}
    		else
    		{
    			throw new NullPointerException("Nothing to pop");
    		}
        }

        public T top() throws NullPointerException
        {
    	    
    		    if(head != null)
    		    {
    		        return(head.value);
    		    }
    		    else
    		    	throw new NullPointerException("head is null");
        }
        
        public void printStack(SLNode n)//, int count)
        {
        		if(n.next != null)
        		{
        			printStack(n.next);//,count);
        		}
		System.out.println(n.index+" : "+n.value);
        }
        
//        public void printStack_i(SLNode n)
//        {
//        		while(n.next != null){
//        			System.out.println(n.index + " : " + n.value);
//        			n = n.next;
//        		}
//        		System.out.println(n.index + " : " + n.value);
//        }

        public boolean IsEmpty()
    	{
    		return (head == null);
        }
        
        public int getSize() //assumes at least one node in stack
        {
        	    int count=1;
            SLNode curs = head;

            // count how many times we can hop references
            while (curs.next != null)
            {
                curs = curs.next;
                count++;
            }

            return count;
        }

        public void clear()
    	{
    		head = null;
        }
        
    }
