/**
 * @author CJ
 * What is the growth rate of this method, in big-O notation? 
 * 		Linear growth, or O(n)
 * What constitutes the input size for the big-O notation?
 * 		The input size (n) would be the amount of circles we look through to see whether or not they contain the point.
 */

import java.awt.geom.*;
import java.util.ArrayList;
import java.util.Iterator;

public class CircleCollector { //Linear growth -> O(n)
	
	private ArrayList<Ellipse2D> circles;
	//private Rectangle2D point;
	private Point2D point;
	
	public CircleCollector()
	{
		circles.clear();
	}
	
	void add(Ellipse2D e)
	{
		circles.add(e); //O(n)
	}
	
	void add(double x, double y, double radius)
	{
		double newX = x - radius / 2.0;
		double newY = y - radius / 2.0;
		
		Ellipse2D.Double e = new Ellipse2D.Double(newX, newY, radius, radius);
		circles.add(e); //O(n)
	}
	void setPoint(Point2D p)
	{
		point = p;
	}
	void setPoint(double x, double y)
	{
		point = new Point2D.Double(x,y);
	}
	
	double containingCircles()
	{
		int count = 0;
		
		for(int i = 0; i < circles.size(); i++)
		{
			if(circles.get(i).contains(point)) //O(1) + O(n) = O(n)
				count++;
		}
		
		return count;
	}

	public List<Ellipse2D> containingCircles (double x, double y) //added after test // /* O(n) */
	{
		Point2D p = new Point2D(x,y);
		List<Ellipse2D> list = new List<Ellipse2D>();
		for ( int i = 0; i < circles.size(); i++)
		{
			if(circles.get(i).contains(p))
				list.add(circles.get(i));
		}
		return list;
	}

}
