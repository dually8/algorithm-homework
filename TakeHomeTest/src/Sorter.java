/**
 * 
 * @author CJ
 * 
 * What is the time cost (big-O) of using this mechanism to sort N random String's assuming they are distributed evenly over the 26 letters of the alphabet?
 * 		The time cost would be O(log n) + O(n).
 * How would performance suffer if all letters started with the same letter?
 * 		 Performance didn't suffer very much during testing.
 * How would performance suffer if we just used one big TreeSet for the whole problem?
 *		Performance seemed to not suffer significantly during testing.  I was unable to have the ArrayList sorted with each TreeSet, so
 *		that factors in to having all of the strings actually sorted.
 */

import java.util.*;

public class Sorter 
{
	
	private ArrayList<TreeSet<String>> list;
	private TreeSet<String> tset;
	
	
	public Sorter() 
	{
		list = new ArrayList<TreeSet<String>>();
	}
	
	void addTreeSet(TreeSet<String> s)
	{
		list.add(s); //O(n)
	}
	
	void print()
	{
		System.out.println(this.list.toString());
	}
	
	public static void main(String args[])
	{
		ArrayList<Long> sorterTimes = new ArrayList<Long>();
		ArrayList<Long> bulkTimes = new ArrayList<Long>();
		for(int i = 0; i < 10; i++)
	 	{
			Sorter s = new Sorter();
			long start = System.nanoTime();
			TreeSet<String> a = new TreeSet<String>();
			a.add("avery"); //O(log n)
			a.add("allison"); //O(log n)
			TreeSet<String> b = new TreeSet<String>();
			b.add("adam");
			b.add("benny");
			b.add("zzzzzzzzzzz");
			b.add("xxxxxxxxxxxx");
			b.add("tanisisthebest");
			s.addTreeSet(a);
			s.addTreeSet(b);		
			s.print();
			long end = System.nanoTime() - start;
			System.out.println("\tSorter: "+end);
			sorterTimes.add(end);
			// start = System.nanoTime();
			// end = System.nanoTime() - start;
			
			start = System.nanoTime();
			TreeSet<String> c = new TreeSet<String>();
			c.addAll(a);
			c.addAll(b);
			System.out.println(c.toString());
			end = System.nanoTime() - start;
			System.out.println("\tBulk: "+end);
			bulkTimes.add(end);
			
			start = System.nanoTime();
			TreeSet<String> d = new TreeSet<String>();
			d.add("zzzzzzzzzz");
			d.add("zzzzz");
			d.add("zzzzzzzz");
			d.add("zzzzzzzzzzzzzzz");
			System.out.println(d.toString());
			end = System.nanoTime() - start;
			System.out.println("\tBulk 2: "+end);
			bulkTimes.add(end);
			
			start = System.nanoTime();
			c.addAll(d);
			System.out.println(c.toString());
			end = System.nanoTime() - start;
			System.out.println("\tBulk 3: "+end);
			bulkTimes.add(end);
	 	}
		
		long averageSort = 0;
		for(int i = 0; i < sorterTimes.size(); i++)
			averageSort += sorterTimes.get(i);
		averageSort = averageSort / sorterTimes.size();
		
		long averageBulk = 0;
		for(int i = 0; i < bulkTimes.size(); i++)
			averageBulk += bulkTimes.get(i);
		averageBulk = averageBulk / bulkTimes.size();
		
		System.out.println("Mean Sort:\t" + averageSort);
		System.out.println("Mean Bulk:\t" + averageBulk);
	}

}
