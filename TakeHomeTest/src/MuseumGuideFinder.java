/**
 * @author CJ
 * 
 * What is the time cost in big-O of adding 1 new visitor to the shortest list?
 * 		The time would be linear, or O(n).
 */
import java.util.LinkedList;


public class MuseumGuideFinder 
{

	private LinkedList<Integer> guides;
	private final int NUM_GUIDES = 10;
	private final int MAX_IN_GROUP = 20;
	private int totalVisitors;
	
	public MuseumGuideFinder() 
	{
		for(int i = 0; i < 5; i++) //O(1)
			guides.add(0);
		totalVisitors = 0;
	}
	
	void addVisitors( int i )
	{
		totalVisitors += i;
		this.addVisitorsToGuide(i); //O(n)
	}
	
	private void addVisitorsToGuide( int i ) //O(1) + O(1) + O(n) = O(n)
	{
		int g = this.yourGuide();//O(n)
		Integer ii = i;
		guides.remove(g); //O(1)
		guides.add(g, ii); //O(1)
	}
	
	void removeVisitorsFromGuide( int i, int v ) //O(n)
	{
		Integer vv = v;
		Integer foo = guides.get(i); //O(n)
		if(foo <= 0)
			return;
		if((foo - vv) < 0)
			foo = 0;
		else
			foo -= vv;
		guides.remove(i); //O(1)
		guides.add(i, foo); //O(1)
		
		totalVisitors -= v;
	}
	
	int yourGuide() //O(n)
	{
		int guideNumber = 0;
		int min = MAX_IN_GROUP; 
		
		for (int i = 0; i < NUM_GUIDES; i++)
		{
			if(guides.get(i).intValue() <= min) //O(n)
				guideNumber = i;
		}
		
		return guideNumber;
	}

}
