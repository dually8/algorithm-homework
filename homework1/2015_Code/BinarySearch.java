public class BinarySearch {

    public long totalTime;
    public long begin;
    public long end;
    private double[] array;

    public BinarySearch(double[] a){
        array = a;
        begin = System.nanoTime();
        totalTime = 0;
    }

    public long getTotalTime(){
        totalTime = end - begin;
        return totalTime;
    }
    /**
     * Searches for key in sorted array a[].
     * @param key the search key
     * @return index of key in array a[] if present; -1 if not present
     */
        public int searchFor(double key) {
        int left = 0;
        int right = array.length - 1;
        begin = System.nanoTime();
        while (left <= right) {
            // Key is in a[left < key < right] or not present
            int target = left + (right - left) / 2;
            if(key < array[target])
                right = target - 1;
            else if (key > array[target])
                left = target + 1;
            else {
                end = System.nanoTime();
                return target;
            }
        }
        end = System.nanoTime();
        return -1;
    }
}
