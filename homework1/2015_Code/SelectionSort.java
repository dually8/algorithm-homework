public class SelectionSort {

    private double[] ar;
    private long begin;
    private long end;
    private long totalTime;

    public SelectionSort(double [] array){
        ar = array;
        begin = 0;
        totalTime = 0;
        end = 0;
    }

    public void sort(){
        int i, j, k;
        double temp = 0;
        begin = System.nanoTime();
        for (i = ar.length - 1; i > 0; i--){
            k = 0;
            for(j = 1; j <= i; j++){
                if(ar[j] < ar[k])
                    k = j;
            }
            temp = ar[k];
            ar[k] = ar[i];
            ar[i] = temp;
        }
        end = System.nanoTime();
    }

    public long getTotalTime(){
        totalTime = end - begin;
        return totalTime;
    }
}
