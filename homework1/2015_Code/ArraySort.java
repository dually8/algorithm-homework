public class ArraySort {

    public long totalTime;
    public long begin;
    public long end;
    private double[] array;

    public ArraySort(double[] a)
    {
        array = a;
        totalTime = 0;
    }

    public long getTotalTime()
    {
        totalTime = end - begin;
        return totalTime;
    }

    public void sort()
    {
        begin = System.nanoTime();
        java.util.Arrays.sort(array);
        end = System.nanoTime();
    }
}
