import java.util.Arrays;

public class ArrayGenerator {

    private double[] array;

    public ArrayGenerator(int size){
        array = new double[size];
        for(int i = 0; i < size; i++){
            array[i] = Math.random();
        }
    }

    public double[] getArray(){
        return array;
    }

    public void sortArray(){
        Arrays.sort(array);
    }
}
