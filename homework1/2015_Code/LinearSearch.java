public class LinearSearch
{
    public long totalTime;
    public long begin;
    public long end;
    private double[] anArray;

    public LinearSearch(double[] a)
    {
        anArray = a;
        totalTime = 0;
        begin = totalTime;
    }

    public long getTotalTime()
    {
        totalTime = end - begin;
        return totalTime;
    }

    public int search(double target)
    {
        begin = System.nanoTime();
        for(int i = 0; i < anArray.length; ++i)
        {
            if(anArray[i] == target)
            {
                end = System.nanoTime();
                return i;
            }
        }
        end = System.nanoTime();
        return -1;
    }
}
