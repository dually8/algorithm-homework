import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class Tester {

    public static double[][] GenerateMatrix(int size){
        double[][] matrix = new double[size][size];
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                matrix[i][j] = 2;//Math.random();
            }
        }
        return matrix;
    }

    public static void main(String[] args){

        // Build LinearSearch
        for(int i = 100; i < 1E6; i *= 2){
            ArrayGenerator arrayGen = new ArrayGenerator(i);
            LinearSearch linearSearch = new LinearSearch(arrayGen.getArray());
            linearSearch.search(-1);
            System.out.println("Linear Time:" + linearSearch.getTotalTime());
        }
        // Build BinarySearch
        for(int i = 100; i < 1E6; i *= 2){
            ArrayGenerator arrayGen = new ArrayGenerator(i);
            arrayGen.sortArray();
            BinarySearch binarySearch = new BinarySearch(arrayGen.getArray());
            binarySearch.searchFor(-1);
            System.out.println("Binary Time: " + binarySearch.getTotalTime());
        }
        // Build SelectionSort
        for(int i = 100; i < 1E6; i *= 2){
            ArrayGenerator arrayGen = new ArrayGenerator(i);
            SelectionSort selectionSort = new SelectionSort(arrayGen.getArray());
            selectionSort.sort();
            System.out.println("Selection Time: " + selectionSort.getTotalTime() +
                    "\tSeconds: " + TimeUnit.SECONDS.convert(selectionSort.getTotalTime(), TimeUnit.NANOSECONDS));
        }
        // Build Arrays.sort
        for(int i = 100; i < 1E6; i *= 2){
            ArrayGenerator arrayGen = new ArrayGenerator(i);
            ArraySort arraySort = new ArraySort(arrayGen.getArray());
            arraySort.sort();
            System.out.println("JavaSort Time: " + arraySort.getTotalTime());
        }

        // Build MatrixSum
        try {
            for (int i = 100; i < 1E6; i *= 2) {
                double[][] matrix1 = GenerateMatrix(i / 100);
                double[][] matrix2 = GenerateMatrix(i / 100);
                MatrixAdder matrixAdder = new MatrixAdder(matrix1, matrix2);
                matrixAdder.addMatrix();
                System.out.println("Matrix Time: " + matrixAdder.getTotalTime() +
                        "\tSeconds: " + TimeUnit.SECONDS.convert(matrixAdder.getTotalTime(), TimeUnit.NANOSECONDS));
            }
        } catch (java.lang.OutOfMemoryError ex){
            System.out.println("Error:\t" + ex.getMessage());
        }
    }
}
