
public class MatrixAdder
{
	private double[][] matrix1;
	private double[][] matrix2;
	private double[][] matrix3;
	public long totalTime;
	public long begin;
	public long end;
	
	public MatrixAdder(double[][] a, double[][] b)
	{
		matrix1 = a;
		matrix2 = b;
		matrix3 = a;
		totalTime = 0;
		begin = System.nanoTime();
	}
	
	public double[][] getMatrix()
	{
		return matrix3;
	}
	
	public long getTotalTime()
	{
		totalTime = end - begin;
		return totalTime;
	}
	
	public void addMatrix()
	{
		//matrix1 + matrix2 = matrix3
		for(int i = 0; i < matrix1.length; i++)
		{
			for(int j = 0; j < i; j++)
			{
				matrix3[i][j] = matrix1[i][j] + matrix2[i][j];
			}
		}
		end = System.nanoTime();
		//return;
	}

}
