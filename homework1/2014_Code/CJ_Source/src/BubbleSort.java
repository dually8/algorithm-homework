/**
 * 
 * @author cj
 *
 */
public class BubbleSort 
{
	public long totalTime;
	public long begin;
	public long end;
	private double[] anArray;
	
	public BubbleSort(double[] a)
	{
		anArray = a;
		totalTime = 0;
		begin = totalTime;
		
	}
	
	public long getTotalTime()
	{
		totalTime = end - begin;
		return totalTime;
	}
	
	public void bubbleSort(double[] a)
	{
		anArray = a;
		boolean swapped = true;
		int j = 0;
		double temp = 0;
		begin = System.nanoTime();
		while(swapped)
		{
			swapped = false;
			j++;
			for(int i = 0; i < anArray.length - j; i++)
			{
				if(anArray[i] > anArray[i + 1])
				{
					temp = anArray[i];
					anArray[i] = anArray[i+1];
					anArray[i+1] = temp;
					swapped = true;
					end = System.nanoTime();
					return; //anArray;
				}
			}
			end = System.nanoTime();
			return;// anArray;
		}
		end = System.nanoTime();
		return;// anArray;
	}
	
}