/**
 * 
 * @author cj
 *
 */
public class BinarySearch 
{
	
	public long totalTime;
	public long begin;
	public long end;
	private double[] anArray;
	
	public BinarySearch(double[] a)
	{
		anArray = a;
		begin = System.nanoTime();
		totalTime = 0;
	}
	
	public long getTotalTime()
	{
		totalTime = end - begin;
		return totalTime;
	}
	
	public int binSearch_Recursive(double[] sortedArray, int first, int last, double key)
	{
		if(first <= last)
		{
			int mid = (first + last) / 2;
			if(key == sortedArray[mid])
			{
				end = System.nanoTime();
				return mid; //target found
			}
			else if(key < sortedArray[mid])
			{
				binSearch_Recursive(sortedArray, first, mid-1, key);
			}
			else
			{
				return binSearch_Recursive(sortedArray, mid+1, last, key);
			}
		}
		end = System.nanoTime();
		return -(first + 1); //failed to find target
	}
}
