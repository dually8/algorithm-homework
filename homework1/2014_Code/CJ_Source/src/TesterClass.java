/**
 * 
 * @author cj
 *
 */
public class TesterClass 
{

	
	public static void main(String[] args) 
	{
		//long t1 = System.nanoTime();
		GenerateArray anArray = new GenerateArray(10);
		double[] theArray = anArray.getArray();
		
		double[][] matrix1 = new double[2][2];
		matrix1[0][0] = 2;
		matrix1[0][1] = 2;
		matrix1[1][0] = 2;
		matrix1[1][1] = 2;
		double[][] matrix2 = new double[2][2];
		matrix2[0][0] = 2;
		matrix2[0][1] = 2;
		matrix2[1][0] = 2;
		matrix2[1][1] = 2;
		
		MatrixAdder mAdd = new MatrixAdder(matrix1,matrix2);
		mAdd.addMatrix();
		//double[][] matrix3 = mAdd.getMatrix();
		System.out.println("Matrix: " + mAdd.getTotalTime());
		
		
		JavaSort jSort = new JavaSort(theArray);
		jSort.JSort(theArray);
		System.out.println("JSort :" + jSort.getTotalTime());
		LinearSearch LS = new LinearSearch(theArray);
		LS.linSearch(Math.random());
		System.out.println("Linear Search :" + LS.getTotalTime());
		BubbleSort bSort = new BubbleSort(theArray);
		bSort.bubbleSort(theArray);
		System.out.println("Bubble Sort :" + bSort.getTotalTime());
		BinarySearch BS = new BinarySearch(theArray);
		BS.binSearch_Recursive(theArray, 0, 1, Math.random());
		System.out.println("Binary Search :" + BS.getTotalTime());
		
		
		
		//long t2 = System.nanoTime();
		
		//System.out.println(t2-t1);

	}

}
