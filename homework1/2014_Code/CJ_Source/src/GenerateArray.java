/**
 * 
 * @author cj
 *
 */
public class GenerateArray 
{
	private double[] theArray;
	
	public GenerateArray(int size)
	{
		theArray = new double[size];
		for(int i = 0; i < size; i++)
			theArray[i] = Math.random();
		
	}
	
	public double[] getArray()
	{
		return theArray;
	}

}
