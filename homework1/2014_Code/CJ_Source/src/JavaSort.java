/**
 * 
 * @author cj
 *
 */
public class JavaSort 
{
	public long totalTime;
	public long begin;
	public long end;
	private double[] anArray;
	
	public JavaSort(double[] a)
	{
		anArray = a;
		totalTime = 0;
		begin = System.nanoTime();
	}
	
	public long getTotalTime()
	{
		totalTime = end - begin;
		return totalTime;
	}
	
	public void JSort(double[] arr)
	{
		anArray = arr;
		java.util.Arrays.sort(anArray);
		end = System.nanoTime();
		//totalTime = end - begin;
		return;// anArray;
	}
}
