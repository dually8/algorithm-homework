package hw1_code;

import java.util.Random;



public class ArraySearch {
	private long begin;
	private long endTime;
	private long totalTime;
	
	//only works if using a sorted array
	public boolean binarySearch(double[] array, double target, int left, int right){
		begin = System.nanoTime();
		while(left < right)
		{
			int mid = (left + right)/2;
			if(array[mid] < target)
			{
				left = mid + 1;
			}
			else if(target < array[mid]){
				right = mid;
			}
			else{
				return true;
			}
		}
		endTime = System.nanoTime();
		totalTime = endTime - begin;
		System.out.println("binary search time is: " + totalTime);
		return false;
	}
	
	public void linearSearch(double[] array, double target){
		begin = System.nanoTime();
		for(int i = 0; i < array.length; i++){
			if(array[i]==target){
				endTime = System.nanoTime();
				totalTime = endTime - begin;
				System.out.println("linear search time: "+ totalTime);
				return;
			}
		}
		endTime = System.nanoTime();
		totalTime = endTime - begin;
		System.out.println("linear search time: "+ totalTime);
		return;
	}
}
