package hw1_code;

public class MatrixSum{
	
	
	public double[][] GenerateMatrix(int size)
	{
		double [][] aMatrix = new double[size][size];
		for(int i = 0; i < size; i++)
		{
				for(int j = 0; j < size; j++)
				{
					aMatrix[i][j] = Math.random();
				}
		}
		return aMatrix;
	}
	
	public void addMatrix(int n, double[][] a , double[][] b){
		long beginTime = System.nanoTime();
		for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				a[i][j] += b[i][j];
			}
		}
		long endTime = System.nanoTime();
		System.out.println("Matrix add time is: " + (endTime-beginTime));
	}
}
