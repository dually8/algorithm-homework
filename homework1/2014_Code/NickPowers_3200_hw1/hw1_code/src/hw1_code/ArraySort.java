package hw1_code;

public class ArraySort {
	double[] sortedArray; 
	public void jSort(double[] array){
		long begin = System.nanoTime();
		java.util.Arrays.sort(array); 
		long end = System.nanoTime();
//		System.out.println("the jsort time is : " + (end-begin));
		sortedArray = array;
	}
	public void bubbleSort(double[] array){
                boolean swapped = true;
                int j = 0;
                double temp = 0;
                long begin = System.nanoTime();
                while(swapped)
                {
                        swapped = false;
                        j++;
                        for(int i = 0; i < array.length - j; i++)
                        {
                                if(array[i] > array[i + 1])
                                {
                                        temp = array[i];
                                        array[i] = array[i+1];
                                        array[i+1] = temp;
                                        swapped = true;
                                        long end = System.nanoTime();
                                        long totalTime = end - begin;
 //                                       System.out.println("the for bubble sort is: " +totalTime);
                                       // return; 
                                }
                        }
                        long end = System.nanoTime();
                        long totalTime = end - begin;
//                        System.out.println("the for bubble sort is: " +totalTime);
                      //  return;
                }
                long end = System.nanoTime();
                long totalTime = end - begin;
//                System.out.println("the for bubble sort is: " +totalTime);
                //return;
        }
	public double[] getArray(){
		return sortedArray;
	}
}
